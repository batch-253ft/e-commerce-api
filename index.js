// ========== MODULES (NPM) ==========
const express = require ("express");
const mongoose = require ("mongoose");
const cors = require ("cors");
// =============================

// ========== MODULES (LOCAL) ==========
const userRoute = require ("./routes/userRoute");
const productRoute = require ("./routes/productRoute");
// ===================================

// ========== DATABASE CONNECTION ==========
const port = process.env.PORT || 4000
let db = mongoose.connection;
mongoose.connect ("mongodb+srv://admin:admin123@batch253-tamayo.wevsze0.mongodb.net/capstone-2?retryWrites=true&w=majority", 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}	
);
db.once ("open", () => console.log (`Now connected to: MongoDB Atlas`));
// =========================================

// ========== Express.js INITIALIZATION ==========
const app = express();
app.use (cors());
app.use (express.json());
app.use (express.urlencoded ({extended: true}));
// ===============================================

// ========== ROUTES CONNECTION ==========
app.use ("/user", userRoute);
app.use ("/admin", productRoute);
// ============================

// ========== API CONNECTION ==========
if (require.main === module) {
	app.listen (port, () => console.log (`API is now online on port ${port}`));
};
// ====================================